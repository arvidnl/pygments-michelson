# Pygments Lexer for Michelson contracts

A lexer class for Michelson contracts.

## Installing

From pip:

```
# pip install pygments-michelson
```


## Usage

### On the command line

```bash
$ pygmentize contract.tz
```

### From python script to generate HTML

```python
from pygments import highlight
from pygments_michelson import MichelsonLexer
from pygments.formatters import HtmlFormatter

code = 'parameter unit; storage unit; code { CDR; NIL operation; PAIR };'
print(highlight(code, MichelsonLexer(), HtmlFormatter()))
```

## Running tests

Set environment variable ```TEZOS_HOME``` and run ```make test``` to
test lexer over all contracts in ```TEZOS_HOME/tests_python/contracts```.
