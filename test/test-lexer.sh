#!/bin/bash

CONTRACTS_DIR=$1

for i in `ls $CONTRACTS_DIR/*/*.tz`; do
    if x=$( pygmentize -l src/pygments_michelson/michelson_lexer.py:MichelsonLexer -f raw -x $i | grep Error ); then
		echo "Unlexed parser token in error in $i: $x"
		exit 1
    else
        echo "$i OK"
    fi
done


echo "All contracts successfully lexed."
